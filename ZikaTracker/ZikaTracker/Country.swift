//
//  Country.swift
//  ZikaTracker
//
//  Created by Administrator on 7/16/16.
//  Copyright © 2016 Bryan Ratledge. All rights reserved.
//

import UIKit

class Country {
    var name: String?
    var localCases: Double?
    var travelCases: Double?
    var localPercentStr: String?
    var travelPercentStr: String?
    var activeWithZika: Bool = false
    var latVal: Double?
    var longVal: Double?
    var containsTerritories: Bool = false
    var territories = [Territory?]()
    var timeStamp: NSDate?
}

