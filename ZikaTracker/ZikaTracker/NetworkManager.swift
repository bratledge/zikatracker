//
//  NetworkManager.swift
//  ZikaTracker
//
//  Created by Administrator on 7/31/16.
//  Copyright © 2016 Bryan Ratledge. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner

class NetworkManager {
    private init() {}
    static let sharedInstance = NetworkManager()
    //var baseURL = "http://localhost:3005"
    var baseURL = "http://52.25.102.43:3005"
    var unitedStatesCases = [Country?]()
    var zikaCountries = [ZikaCountries?]()
    var zikaPregnancyCases = [ZikaPregnancyCase?]()
    var zikaNews = [ZikaNews?]()
    
    var loadedCases = false
    var loadedCountries = false
    var loadedPregnancies = false
    var loadedNews = false
    
    func errorConnecting(msg: String) {
        let alert = UIAlertController(title: "Connection Error", message: "Error connecting to satellites...", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default,
            handler: {
                (alert: UIAlertAction!) in print("Connection error")
            }))
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window?.rootViewController?.presentViewController(alert, animated: true, completion: {})
        
    }
    
    func refreshZikaData(sender: AnyObject?) {
        let timeNow:NSDate = NSDate()
        let lastUsedDate:NSDate? = NSUserDefaults.standardUserDefaults().objectForKey("lastRefreshedApp") as! NSDate?
        if (lastUsedDate == nil)
        {
            self.performRefresh(sender)
        } else {
            if (timeNow.timeIntervalSinceDate(lastUsedDate!) > 10.0) {
                self.performRefresh(sender)
            } else {
                SwiftSpinner.showWithDuration(2.5, title: "No new data", animated: false)
            }
        }
    }
    
    func performRefresh(sender: AnyObject?) {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(NSDate(), forKey: "lastRefreshedApp")
        
        loadedCases = false
        loadedCountries = false
        loadedPregnancies = false
        loadedNews = false
        unitedStatesCases = [Country?]()
        zikaCountries = [ZikaCountries?]()
        zikaPregnancyCases = [ZikaPregnancyCase?]()
        zikaNews = [ZikaNews?]()
        
        SwiftSpinner.show("talking to satellites...")
        
        Alamofire.request(.GET, "\(baseURL)/zika", parameters: ["": ""])
            .responseJSON { response in
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let JSON = response.result.value {
                    for i in 0...JSON.count - 1 {
                        let usaCaseSet = Country()
                        usaCaseSet.activeWithZika = true
                        usaCaseSet.containsTerritories = true
                        usaCaseSet.name = "United States"
                        
                        
                        let dateStr = JSON.objectAtIndex(i).objectForKey("timeStamp") as? String
                        let date = NSDate(timeIntervalSince1970: Double(dateStr!)!)
                        usaCaseSet.timeStamp = date
                        
                        for j in 0...JSON.objectAtIndex(i).objectForKey("zikaCases")!.count - 1 {
                            let territory = Territory()
                            territory.name = JSON.objectAtIndex(i).objectForKey("zikaCases")?.objectAtIndex(j).objectForKey("state") as? String
                            territory.travelCases = Double(JSON.objectAtIndex(i).objectForKey("zikaCases")!.objectAtIndex(j).objectForKey("travelCases") as! String)
                            territory.travelPercentStr = JSON.objectAtIndex(i).objectForKey("zikaCases")?.objectAtIndex(j).objectForKey("travelPercent") as? String
                            territory.localCases = Double(JSON.objectAtIndex(i).objectForKey("zikaCases")!.objectAtIndex(j).objectForKey("localCases") as! String)
                            territory.localPercentStr = JSON.objectAtIndex(i).objectForKey("zikaCases")?.objectAtIndex(j).objectForKey("localPercent") as? String
                            usaCaseSet.territories.append(territory)
                        }
                        
                        self.unitedStatesCases.append(usaCaseSet)
                        
                        
                        
                        //JSON.objectAtIndex(1).objectForKey("zikaCases")!.objectAtIndex(0)!.objectForKey("name")
                        
                        //JSON.objectAtIndex(1).objectForKey("zikaCases")!.objectAtIndex(0).objectForKey("country") as! String (String) $R13 = "United States"
                        
                        
                    }
                    self.loadedCases = true
                    if (self.loadedCountries && self.loadedCases && self.loadedPregnancies && self.loadedNews) {
                        SwiftSpinner.hide()
                        self.redrawVCAfterLoad(sender)
                    }
                } else {
                    NetworkManager.sharedInstance.errorConnecting("")
                    self.loadedCases = true
                    if (self.loadedCountries && self.loadedCases && self.loadedPregnancies && self.loadedNews) {
                        SwiftSpinner.hide()
                    }
                    return
                }
                
                //Parse JSON
                
        }
        
        Alamofire.request(.GET, "\(baseURL)/zikaCountries", parameters: ["": ""])
            .responseJSON { response in
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let JSON = response.result.value {
                    for i in 0...(JSON as! NSArray).count - 1 {
                        let zikaCountry: ZikaCountries = ZikaCountries()
                        let dateStr = (JSON as! NSArray).objectAtIndex(i).objectForKey("timeStamp") as! String
                        let date = NSDate(timeIntervalSince1970: Double(dateStr)!)
                        zikaCountry.timeStamp = date
                        for j in 0...(JSON.objectAtIndex(i).objectForKey("zikaCountries") as! NSArray).count - 1 {
                            let zikaCountriesCountry: ZikaCountriesCountry = ZikaCountriesCountry()
                            zikaCountriesCountry.name = ((JSON.objectAtIndex(i).objectForKey("zikaCountries") as! NSArray).objectAtIndex(j) as! NSDictionary).objectForKey("name") as? String
                            zikaCountriesCountry.continent = ((JSON.objectAtIndex(i).objectForKey("zikaCountries") as! NSArray).objectAtIndex(j) as! NSDictionary).objectForKey("continent") as? String
                            
                            if let centerLat = ((JSON.objectAtIndex(i).objectForKey("zikaCountries") as! NSArray).objectAtIndex(j) as! NSDictionary).objectForKey("centerLat") as? String {
                                zikaCountriesCountry.latVal = Double(centerLat)
                            }
                            
                            if let centerLong = ((JSON.objectAtIndex(i).objectForKey("zikaCountries") as! NSArray).objectAtIndex(j) as! NSDictionary).objectForKey("centerLong") as? String {
                                zikaCountriesCountry.longVal = Double(centerLong)
                            }
                            zikaCountry.zikaCountriesCountries.append(zikaCountriesCountry)
                        }
                        self.zikaCountries.append(zikaCountry)
                    }
                    //print("JSON: \(JSON)")
                    self.loadedCountries = true
                    if (self.loadedCountries && self.loadedCases && self.loadedPregnancies && self.loadedNews) {
                        SwiftSpinner.hide()
                        self.redrawVCAfterLoad(sender)
                    }
                } else {
                    NetworkManager.sharedInstance.errorConnecting("")
                    self.loadedCountries = true
                    if (self.loadedCountries && self.loadedCases && self.loadedPregnancies && self.loadedNews) {
                        SwiftSpinner.hide()
                    }
                    return
                }
        }
        
        Alamofire.request(.GET, "\(baseURL)/zikaPregnancyCases", parameters: ["":""])
            .responseJSON { response in
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                
                if let JSON = response.result.value {
                    for i in 0...(JSON as! NSArray).count - 1 {
                        
                        let zikaPregnancy: ZikaPregnancyCase = ZikaPregnancyCase()
                        
                        let dateStr = JSON.objectAtIndex(i).objectForKey("timeStamp") as? String
                        let date = NSDate(timeIntervalSince1970: Double(dateStr!)!)
                        zikaPregnancy.timeStamp = date
                        zikaPregnancy.totalPregnancyCasesUSA = Int(JSON.objectAtIndex(i).objectForKey("usaCases")! as! String)!
                        //remove ,'s
                        var casesString = JSON.objectAtIndex(i).objectForKey("usaTerritoryCases")! as! String
                        casesString = casesString.stringByReplacingOccurrencesOfString(",", withString: "")
                        zikaPregnancy.totalPregnancyCasesTerritory = Int(casesString)!
                        
                        self.zikaPregnancyCases.append(zikaPregnancy)
                    }
                    //print("JSON: \(JSON)")
                    self.loadedPregnancies = true
                    if (self.loadedCountries && self.loadedCases && self.loadedPregnancies && self.loadedNews) {
                        SwiftSpinner.hide()
                        self.redrawVCAfterLoad(sender)
                    }
                } else {
                    NetworkManager.sharedInstance.errorConnecting("Error loading pregnancy data")
                    self.loadedPregnancies = true
                    if (self.loadedCountries && self.loadedCases && self.loadedPregnancies && self.loadedNews) {
                        SwiftSpinner.hide()
                    }
                    return
                }
                
        }
        
        Alamofire.request(.GET, "\(baseURL)/zikaNews", parameters: ["":""])
            .responseJSON { response in
                //print(response.request)  // original URL request
                //print(response.response) // URL response
                //print(response.data)     // server data
                //print(response.result)   // result of response serialization
                self.zikaNews.removeAll()
                if let JSON = response.result.value {
                    for i in 0...(JSON as! NSArray).count - 1 {
                        
                        let zikaNews: ZikaNews = ZikaNews()
                        
                        let dateStr = JSON.objectAtIndex(i).objectForKey("timeStamp") as? String
                        let date = NSDate(timeIntervalSince1970: Double(dateStr!)!)
                        zikaNews.timeStamp = date
                        zikaNews.img_url = JSON.objectAtIndex(i).objectForKey("image_url") as? String
                        zikaNews.map_image = JSON.objectAtIndex(i).objectForKey("map_image") as? String
                        zikaNews.title = JSON.objectAtIndex(i).objectForKey("title") as? String
                        zikaNews.type = JSON.objectAtIndex(i).objectForKey("type") as? String
                        zikaNews.web_url = JSON.objectAtIndex(i).objectForKey("web_url") as? String
                        
                        self.zikaNews.append(zikaNews)
                    }
                    //print("JSON: \(JSON)")
                    self.loadedNews = true
                    if (self.loadedCountries && self.loadedCases && self.loadedPregnancies && self.loadedNews) {
                        SwiftSpinner.hide()
                        self.redrawVCAfterLoad(sender)
                    }
                } else {
                    NetworkManager.sharedInstance.errorConnecting("Error loading news data")
                    self.loadedNews = true
                    if (self.loadedCountries && self.loadedCases && self.loadedPregnancies  && self.loadedNews) {
                        SwiftSpinner.hide()
                    }
                    return
                }
                
        }
    }
    
    func redrawVCAfterLoad(sender: AnyObject?) {
        
        if (sender is SummaryViewController) {
            (sender as? SummaryViewController)?.reloadData()
        }
        
        if (sender is SearchViewController) {
            (sender as? SearchViewController)?.reloadData()
        }
        
        if (sender is NewsViewController) {
            (sender as? NewsViewController)?.reloadData()
        }
    }
}
