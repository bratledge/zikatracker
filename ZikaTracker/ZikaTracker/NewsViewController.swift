//
//  NewsViewController.swift
//  ZikaTracker
//
//  Created by Administrator on 8/26/16.
//  Copyright © 2016 Bryan Ratledge. All rights reserved.
//

import UIKit
import Foundation
import SDWebImage
import SafariServices


class NewsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SFSafariViewControllerDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    private let reuseIdentifier = "newsCell"
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.alwaysBounceVertical = true
        self.collectionView.bounces = true
        
        refreshControl.tintColor = UIColor.whiteColor()
        refreshControl.addTarget(self, action: #selector(refresh), forControlEvents: .ValueChanged)
        self.collectionView.addSubview(refreshControl)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadData() {
        self.collectionView.reloadData()
    }
    
    func refresh() {
        NetworkManager.sharedInstance.refreshZikaData(self)
        refreshControl.endRefreshing()
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return NetworkManager.sharedInstance.zikaNews.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as UICollectionViewCell
        let titleLbl = cell.viewWithTag(2) as! UILabel
        titleLbl.text = NetworkManager.sharedInstance.zikaNews[indexPath.row]?.title
        let mainImg = cell.viewWithTag(3) as! UIImageView
        mainImg.sd_setImageWithURL(NSURL(string: NetworkManager.sharedInstance.zikaNews[indexPath.row]!.img_url!), placeholderImage: UIImage(contentsOfFile: "splash1"))
        let dateLbl = cell.viewWithTag(4) as! UILabel
        
        let dayTimePeriodFormatter = NSDateFormatter()
        dayTimePeriodFormatter.dateFormat = "M/d/yy"
        let dateString = dayTimePeriodFormatter.stringFromDate(NetworkManager.sharedInstance.zikaNews[indexPath.row]!.timeStamp!)
        
        dateLbl.text = dateString
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(self.collectionView.frame.size.width/2 - 10, 250)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        
        if let urlString = NetworkManager.sharedInstance.zikaNews[indexPath.row]?.web_url {
            if let url = NSURL(string: urlString) {
                let vc = SFSafariViewController(URL: url, entersReaderIfAvailable: true)
                vc.delegate = self
                
                presentViewController(vc, animated: true, completion: nil)
            }
        }
        
    }
    
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
