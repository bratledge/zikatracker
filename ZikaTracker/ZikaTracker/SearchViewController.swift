//
//  SearchViewController.swift
//  ZikaTracker
//
//  Created by Administrator on 6/14/16.
//  Copyright © 2016 Bryan Ratledge. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
import FBAnnotationClustering

//f39331 ORANGE
//103C61 BLUE

class SearchViewController: UIViewController, MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var navSegment: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    
    var annotations = [MKAnnotation]()
    let clusteringManager = FBClusteringManager()
    let refreshControl = UIRefreshControl()
    var locationManager: CLLocationManager!
    
    var statesWithLatLong = [StateWithLatLong]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.parseStatesCSV()
        self.tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        self.mapView.delegate = self
        self.clusteringManager.delegate = self
        reloadMapAnnotations()
        mapView.showsUserLocation = true
        self.tableView.hidden = true
        self.mapView.hidden = false

        
        refreshControl.tintColor = UIColor.whiteColor()
        refreshControl.addTarget(self, action: #selector(refresh), forControlEvents: .ValueChanged)
        self.tableView.addSubview(refreshControl)
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadData() {
        self.tableView.reloadData()
        reloadMapAnnotations()
    }
    
    func refresh() {
        NetworkManager.sharedInstance.refreshZikaData(self)
        refreshControl.endRefreshing()
    }
    
    // MARK: - Map
    /*
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        self.mapView.setRegion(region, animated: true)
    }
 */
    
    func reloadMapAnnotations() {
        self.parseStatesCSV()
        mapView.removeAnnotations(annotations)
        annotations.removeAll()
        
        var minLatitude = Double.infinity
        var maxLatitude = -Double.infinity
        var minLongitude = Double.infinity
        var maxLongitude = -Double.infinity
        if (NetworkManager.sharedInstance.zikaCountries.count > 0) {
            for country in NetworkManager.sharedInstance.zikaCountries[NetworkManager.sharedInstance.zikaCountries.count - 1]!.zikaCountriesCountries {
                
                if (country.latVal != nil && country.longVal != nil) {
                    let latitude = country.latVal!
                    let longitude = country.longVal!
                    
                    minLatitude = min(latitude, minLatitude)
                    maxLatitude = max(latitude, maxLatitude)
                    minLongitude = min(longitude, minLongitude)
                    maxLongitude = max(longitude, maxLongitude)
                    
                    let annotation = ZikaMapAnnotation(coordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude)))
                    annotation.country = country
                    
                    //annotation.coordinate =
                    //annotation.title = professional.name
                    
                    //if let description = professional.specializations?[0].field_specialty?.name {
                    //annotation.leftText = description
                    //}
                    
                    annotations.append(annotation)
                }
            }
            
            self.parseStatesCSV()
            
            for territory in NetworkManager.sharedInstance.unitedStatesCases[NetworkManager.sharedInstance.unitedStatesCases.count - 1]!.territories {
                for i in 0...self.statesWithLatLong.count - 1 {
                    if (self.statesWithLatLong[i].stateName == territory?.name) {
                        territory?.latVal = self.statesWithLatLong[i].latitude
                        territory?.longVal = self.statesWithLatLong[i].longitude
                        territory?.name = self.statesWithLatLong[i].stateName
                    }
                }
                
                
                if (territory?.latVal != nil && territory?.longVal != nil) {
                    let latitude = territory!.latVal!
                    let longitude = territory!.longVal!
                    
                    minLatitude = min(latitude, minLatitude)
                    maxLatitude = max(latitude, maxLatitude)
                    minLongitude = min(longitude, minLongitude)
                    maxLongitude = max(longitude, maxLongitude)
                    
                    let annotation: ZikaStateMapAnnotation = ZikaStateMapAnnotation(coordinate: CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude)))
                    annotation.territory = territory
                    //annotation.territory = territory?.territories
                    annotations.append(annotation)
                    

                }
            }
            
            if annotations.count > 0 {
                clusteringManager.addAnnotations(annotations)
                mapView.showAnnotations(annotations, animated: true)
            }
        }
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation.isKindOfClass(MKAnnotationView){
            return nil
        }
        
        if annotation.isKindOfClass(FBAnnotationCluster) {
            
            let reuseId = "Cluster"
            var clusterView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
            clusterView = ZikaAnnotationClusterView(annotation: annotation, reuseIdentifier: reuseId, options: nil)
            
            return clusterView
            
        }
        
        if annotation.isKindOfClass(ZikaMapAnnotation) {
            var annotationView = self.mapView.dequeueReusableAnnotationViewWithIdentifier("Pin")
            
            if annotationView == nil{
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "Pin")
                annotationView?.canShowCallout = false
            }else{
                annotationView?.annotation = annotation
            }
            (annotationView as! MKPinAnnotationView).pinTintColor = UIColor.blueColor()
            return annotationView
        }
        
        if annotation.isKindOfClass(ZikaStateMapAnnotation) {
            var annotationView = self.mapView.dequeueReusableAnnotationViewWithIdentifier("Pin")
            
            if annotationView == nil{
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "Pin")
                annotationView?.canShowCallout = false
            }else{
                annotationView?.annotation = annotation
            }
            (annotationView as! MKPinAnnotationView).pinTintColor = UIColor.redColor()
            return annotationView
        }
        
        return nil
    }

    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool){
        
        NSOperationQueue().addOperationWithBlock({
            
            let mapBoundsWidth = Double(self.mapView.bounds.size.width)
            
            let mapRectWidth:Double = self.mapView.visibleMapRect.size.width
            
            let scale:Double = mapBoundsWidth / mapRectWidth
            
            let annotationArray = self.clusteringManager.clusteredAnnotationsWithinMapRect(self.mapView.visibleMapRect, withZoomScale:scale)
            
            self.clusteringManager.displayAnnotations(annotationArray, onMapView:self.mapView)
            
        })
        
    }
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        //
        mapView.deselectAnnotation(view.annotation, animated: true)
        
        if (view.annotation!.isKindOfClass(MKAnnotationView)) {
            
        } else if (view.annotation!.isKindOfClass(FBAnnotationCluster)) {
            //Groups of Countries
            let mapPopoverVC: ZikaCountryGroupViewController = self.storyboard!.instantiateViewControllerWithIdentifier("zikaCountryGroupViewController") as! ZikaCountryGroupViewController
            //mapPopoverVC.delegate = self
            mapPopoverVC.modalPresentationStyle = .Popover
            mapPopoverVC.preferredContentSize = CGSizeMake(280,300)
            mapPopoverVC.popoverPresentationController?.delegate = self
            mapPopoverVC.popoverPresentationController?.sourceView = view
            mapPopoverVC.popoverPresentationController?.sourceRect = CGRectMake(14,14,0,0)
            mapPopoverVC.popoverPresentationController?.permittedArrowDirections = .Any
            mapPopoverVC.popoverPresentationController?.backgroundColor = UIColor.whiteColor()
            mapPopoverVC.annotations = (view.annotation! as! FBAnnotationCluster).annotations
            self.presentViewController(mapPopoverVC, animated: true, completion: nil)
        } else if (view.annotation!.isKindOfClass(ZikaMapAnnotation)){
            //Country
            let mapPopoverVC: ZikaCountryPopOverViewController = self.storyboard!.instantiateViewControllerWithIdentifier("zikaCountryPopOverViewController") as! ZikaCountryPopOverViewController
            //mapPopoverVC.delegate = self
            mapPopoverVC.modalPresentationStyle = .Popover
            mapPopoverVC.preferredContentSize = CGSizeMake(280,80)
            mapPopoverVC.popoverPresentationController?.delegate = self
            mapPopoverVC.popoverPresentationController?.sourceView = view
            mapPopoverVC.popoverPresentationController?.sourceRect = CGRectMake(14,14,0,0)
            mapPopoverVC.popoverPresentationController?.permittedArrowDirections = .Any
            mapPopoverVC.popoverPresentationController?.backgroundColor = UIColor.whiteColor()
            mapPopoverVC.country = (view.annotation as! ZikaMapAnnotation).country
            self.presentViewController(mapPopoverVC, animated: true, completion: nil)
        } else if (view.annotation!.isKindOfClass(ZikaStateMapAnnotation)){
            //States
            let mapPopoverVC: ZikaStatePopOverViewController = self.storyboard!.instantiateViewControllerWithIdentifier("zikaStatePopOverViewController") as! ZikaStatePopOverViewController
            //mapPopoverVC.delegate = self
            mapPopoverVC.modalPresentationStyle = .Popover
            mapPopoverVC.preferredContentSize = CGSizeMake(280,80)
            mapPopoverVC.popoverPresentationController?.delegate = self
            mapPopoverVC.popoverPresentationController?.sourceView = view
            mapPopoverVC.popoverPresentationController?.sourceRect = CGRectMake(14,14,0,0)
            mapPopoverVC.popoverPresentationController?.permittedArrowDirections = .Any
            mapPopoverVC.popoverPresentationController?.backgroundColor = UIColor.whiteColor()
            mapPopoverVC.territory = (view.annotation as! ZikaStateMapAnnotation).territory
            self.presentViewController(mapPopoverVC, animated: true, completion: nil)
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        // Return no adaptive presentation style, use default presentation behaviour
        return .None
    }
    
    
    // MARK: - TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let zikaCountries = NetworkManager.sharedInstance.zikaCountries
        let countryCount = NetworkManager.sharedInstance.zikaCountries.count - 1
        if (countryCount > 0) {
            return zikaCountries[countryCount]!.zikaCountriesCountries.count
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("countryCell", forIndexPath: indexPath) as UITableViewCell

        let countryLbl = cell.contentView.viewWithTag(1) as! UILabel
        if (NetworkManager.sharedInstance.zikaCountries.count > 0){
            if let latestCountries = NetworkManager.sharedInstance.zikaCountries[NetworkManager.sharedInstance.zikaCountries.count - 1] {
                var nameText = latestCountries.zikaCountriesCountries[indexPath.row].name
                nameText = nameText!.stringByReplacingOccurrencesOfString("\n", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                //nameText = nameText!.stringByReplacingOccurrencesOfString("Commonwealth of", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
                countryLbl.text = nameText
            }
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func navSegmentChanged(sender: AnyObject) {
        if ((sender as! UISegmentedControl).selectedSegmentIndex == 1) {
            //Table
            self.mapView.hidden = true
            self.tableView.hidden = false
        } else {
            //Map
            self.tableView.hidden = true
            self.mapView.hidden = false
        }
    }
    
    func parseStatesCSV() {
        if let path = NSBundle.mainBundle().pathForResource("StatesLatLong", ofType: "json") {
            do {
                let jsonData = try NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingMappedIfSafe)
                do {
                    let jsonResult: NSArray = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as! NSArray
                    print(jsonResult)
                    self.statesWithLatLong.removeAll()
                    for state in jsonResult {
                        let myState: StateWithLatLong = StateWithLatLong()
                        myState.stateName = state.objectForKey("State") as? String
                        myState.latitude = state.objectForKey("Lat") as? Double
                        myState.longitude = state.objectForKey("Long") as? Double
                        self.statesWithLatLong.append(myState)
                        
                    }
                }
                catch {
                    
                }
            } catch {
                
            }
        }
        
    }
}

class StateWithLatLong: NSObject {
    var stateName: String?
    var latitude: Double?
    var longitude: Double?
}

class ZikaMapAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var country: ZikaCountriesCountry?
    var territory: Territory?
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}

class ZikaStateMapAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var country: ZikaCountriesCountry?
    var territory: Territory?
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}

extension SearchViewController : FBClusteringManagerDelegate {
    
    func cellSizeFactorForCoordinator(coordinator:FBClusteringManager) -> CGFloat{
        return 1.0
    }
    
}
