//
//  SettingsViewController.swift
//  ZikaTracker
//
//  Created by Administrator on 8/11/16.
//  Copyright © 2016 Bryan Ratledge. All rights reserved.
//

import UIKit
import SafariServices

class SettingsViewController: UIViewController, SFSafariViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func learnMorePressed(sender: AnyObject) {
        //http://www.cdc.gov/zika/about/index.html
        
        let url = NSURL(string: "https://www.cdc.gov/zika/about/index.html")
        let vc = SFSafariViewController(URL: url!, entersReaderIfAvailable: true)
        vc.delegate = self
        
        presentViewController(vc, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    
}
