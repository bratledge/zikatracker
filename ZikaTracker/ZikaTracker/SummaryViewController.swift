//
//  SummaryViewController.swift
//  ZikaTracker
//
//  Created by Administrator on 8/11/16.
//  Copyright © 2016 Bryan Ratledge. All rights reserved.
//

import UIKit
import Charts
import GearRefreshControl
import Charts.Swift
class SummaryViewController: UIViewController, ChartViewDelegate, UIScrollViewDelegate {

    var initialDataLoaded = false
    var refreshControl: UIRefreshControl?
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var chartView: LineChartView!
    @IBOutlet weak var pregnancyChartView: LineChartView!
    @IBOutlet weak var lastUpdated: UILabel!
    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var barChartViewTravel: BarChartView!
    @IBOutlet weak var statePieChartView: PieChartView!
    @IBOutlet weak var countriesTotalLbl: UILabel!
    @IBOutlet weak var usLocalCasesTotalLbl: UILabel!
    @IBOutlet weak var usTravelCasesTotalLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        self.setupRefreshControl()
        self.setupCountriesLineChartView()
        self.setupTotalLocalUSCasesBarChartView()
        self.setupTotalTravelUSCasesBarChartView()
        self.setupPregnancyLineChartView()
        self.setupStatesCasesPieChartView()
    }
    
    func setupRefreshControl() {
        
        //refreshControl = GearRefreshControl(frame: self.scrollView.bounds)
        //refreshControl!.bounds = CGRectMake(0, -55, refreshControl!.bounds.size.width, refreshControl!.bounds.size.height)
        //refreshControl!.gearTintColor = UIColor(red: 59/255, green: 186/255, blue: 219/255, alpha: 1)
        //refreshControl!.gearTintColor = UIColor.whiteColor()
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = UIColor.whiteColor()
        refreshControl!.addTarget(self, action: #selector(refresh), forControlEvents: .ValueChanged)
        scrollView.addSubview(refreshControl!)
        scrollView.frame = CGRectMake(0, -55, scrollView.bounds.size.width, scrollView.bounds.size.height)
    }
    
    func setupCountriesLineChartView() {
        chartView.delegate = self
        chartView.descriptionText = "Number of countries"
        chartView.descriptionTextColor = UIColor.whiteColor()
        chartView.pinchZoomEnabled = false
        chartView.doubleTapToZoomEnabled = false
        chartView.drawGridBackgroundEnabled = false
        chartView.xAxis.gridColor = UIColor.whiteColor()
        chartView.leftYAxisRenderer.yAxis?.drawGridLinesEnabled = true
        chartView.leftYAxisRenderer.yAxis?.gridColor = UIColor.whiteColor()
        chartView.leftYAxisRenderer.yAxis?.gridLineWidth = 0.5
        chartView.leftYAxisRenderer.yAxis?.forceLabelsEnabled = true
        chartView.rightYAxisRenderer.yAxis?.drawGridLinesEnabled = false
        chartView.backgroundColor = UIColor.clearColor()
        chartView.legend.form = .Line
        chartView.legend.textColor = UIColor.whiteColor()
        chartView.infoTextColor = UIColor.whiteColor()
        chartView.xAxis.labelTextColor = UIColor.whiteColor()
        chartView.rightAxis.enabled = false
        chartView.leftAxis.labelTextColor = UIColor.whiteColor()
        chartView.leftAxis.valueFormatter = NSNumberFormatter()
        chartView.leftAxis.valueFormatter?.numberStyle = NSNumberFormatterStyle.NoStyle
    }
    
    func setupStatesCasesPieChartView() {
        statePieChartView.delegate = self
        statePieChartView.descriptionText = "Number per State"
        statePieChartView.descriptionTextColor = UIColor.whiteColor()
        statePieChartView.backgroundColor = UIColor.clearColor()
        statePieChartView.infoTextColor = UIColor.whiteColor()
        statePieChartView.drawCenterTextEnabled = true
        statePieChartView.holeColor = UIColor.clearColor()
    }
    
    func setupPregnancyLineChartView() {
        pregnancyChartView.delegate = self
        pregnancyChartView.descriptionText = "Number of pregnancies"
        pregnancyChartView.descriptionTextColor = UIColor.whiteColor()
        pregnancyChartView.pinchZoomEnabled = false
        pregnancyChartView.doubleTapToZoomEnabled = false
        pregnancyChartView.drawGridBackgroundEnabled = false
        pregnancyChartView.xAxis.gridColor = UIColor.whiteColor()
        pregnancyChartView.leftYAxisRenderer.yAxis?.drawGridLinesEnabled = true
        pregnancyChartView.leftYAxisRenderer.yAxis?.gridColor = UIColor.whiteColor()
        pregnancyChartView.leftYAxisRenderer.yAxis?.gridLineWidth = 0.5
        pregnancyChartView.leftYAxisRenderer.yAxis?.forceLabelsEnabled = true
        pregnancyChartView.rightYAxisRenderer.yAxis?.drawGridLinesEnabled = false
        pregnancyChartView.backgroundColor = UIColor.clearColor()
        pregnancyChartView.legend.form = .Line
        pregnancyChartView.legend.textColor = UIColor.whiteColor()
        pregnancyChartView.infoTextColor = UIColor.clearColor()
        pregnancyChartView.xAxis.labelTextColor = UIColor.clearColor()
        pregnancyChartView.rightAxis.enabled = false
        pregnancyChartView.leftAxis.labelTextColor = UIColor.whiteColor()
        pregnancyChartView.leftAxis.valueFormatter = NSNumberFormatter()
        pregnancyChartView.leftAxis.valueFormatter?.numberStyle = NSNumberFormatterStyle.NoStyle
    }
    
    func setupTotalLocalUSCasesBarChartView() {
        barChartView.delegate = self
        barChartView.descriptionText = ""
        barChartView.descriptionTextColor = UIColor.whiteColor()
        barChartView.pinchZoomEnabled = false
        barChartView.doubleTapToZoomEnabled = false
        barChartView.drawGridBackgroundEnabled = false
        barChartView.xAxis.gridColor = UIColor.whiteColor()
        barChartView.leftYAxisRenderer.yAxis?.drawGridLinesEnabled = true
        barChartView.leftYAxisRenderer.yAxis?.gridColor = UIColor.whiteColor()
        barChartView.leftYAxisRenderer.yAxis?.gridLineWidth = 0.5
        barChartView.leftYAxisRenderer.yAxis?.forceLabelsEnabled = true
        barChartView.leftYAxisRenderer.yAxis?.axisMinValue = 0
        barChartView.rightYAxisRenderer.yAxis?.drawGridLinesEnabled = false
        barChartView.legend.textColor = UIColor.whiteColor()
        barChartView.infoTextColor = UIColor.clearColor()
        barChartView.xAxis.labelTextColor = UIColor.whiteColor()
        barChartView.leftAxis.labelTextColor = UIColor.whiteColor()
        barChartView.leftAxis.valueFormatter = NSNumberFormatter()
        barChartView.leftAxis.valueFormatter?.numberStyle = NSNumberFormatterStyle.NoStyle
        barChartView.rightAxis.enabled = false
    }
    
    func setupTotalTravelUSCasesBarChartView() {
        barChartViewTravel.delegate = self
        barChartViewTravel.descriptionText = ""
        barChartViewTravel.descriptionTextColor = UIColor.whiteColor()
        barChartViewTravel.pinchZoomEnabled = false
        barChartViewTravel.doubleTapToZoomEnabled = false
        barChartViewTravel.drawGridBackgroundEnabled = false
        barChartViewTravel.xAxis.gridColor = UIColor.whiteColor()
        barChartViewTravel.leftYAxisRenderer.yAxis?.drawGridLinesEnabled = true
        barChartViewTravel.leftYAxisRenderer.yAxis?.gridColor = UIColor.whiteColor()
        barChartViewTravel.leftYAxisRenderer.yAxis?.gridLineWidth = 0.5
        barChartViewTravel.leftYAxisRenderer.yAxis?.forceLabelsEnabled = true
        barChartViewTravel.leftYAxisRenderer.yAxis?.axisMinValue = 0
        barChartViewTravel.rightYAxisRenderer.yAxis?.drawGridLinesEnabled = false
        barChartViewTravel.legend.textColor = UIColor.whiteColor()
        barChartViewTravel.infoTextColor = UIColor.whiteColor()
        barChartViewTravel.xAxis.labelTextColor = UIColor.whiteColor()
        barChartViewTravel.leftAxis.labelTextColor = UIColor.whiteColor()
        barChartViewTravel.leftAxis.valueFormatter = NSNumberFormatter()
        barChartViewTravel.leftAxis.valueFormatter?.numberStyle = NSNumberFormatterStyle.NoStyle
        barChartViewTravel.rightAxis.enabled = false
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        //refreshControl!.scrollViewDidScroll(scrollView)
    }
    
    override func viewWillAppear(animated: Bool) {
        if (initialDataLoaded == false) {
            self.refresh()
            initialDataLoaded = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refresh() {
        NetworkManager.sharedInstance.refreshZikaData(self)
        refreshControl!.endRefreshing()
    }
    
    func reloadData() {
        self.drawDataForTotalCountries()
        self.drawDataForTotalUSCases()
        self.drawDataForTotalPregnancies()
        self.drawDataForStateCases()
    }
    
    func drawDataForStateCases() {
        
        var values = [ChartDataEntry]()
        var xVals = [String]()
        for i in 0...NetworkManager.sharedInstance.unitedStatesCases[NetworkManager.sharedInstance.unitedStatesCases.count - 1]!.territories.count - 1 {
            if let territory = NetworkManager.sharedInstance.unitedStatesCases[NetworkManager.sharedInstance.unitedStatesCases.count - 1]!.territories[i] {
                print(territory.name!)
                print(territory.localCases!)
                if (territory.localCases! > 0) {
                    let entry = ChartDataEntry(value: territory.localCases!, xIndex: i)
                    xVals.append(territory.name!)
                    values.append(entry)
                }
            }
        }
        
        let set: PieChartDataSet = PieChartDataSet(yVals: values, label: "")
        set.colors = ChartColorTemplates.colorful()
        set.sliceSpace = 2.0;
        let data:PieChartData = PieChartData(xVals: xVals, dataSet: set)
        statePieChartView.transparentCircleRadiusPercent = 0.61;
        self.statePieChartView.data = data
        self.statePieChartView.setNeedsDisplay()
    }
    
    func drawDataForTotalCountries() {
        var yVals = [ChartDataEntry]()
        var xVals = [String?]()
        var latestCountryCount = 0
        for i in 0...NetworkManager.sharedInstance.zikaCountries.count - 1 {
            let xValCountry:ZikaCountries = NetworkManager.sharedInstance.zikaCountries[i]!
            
            let dayTimePeriodFormatter = NSDateFormatter()
            dayTimePeriodFormatter.dateFormat = "M/d/yy"
            let dateString = dayTimePeriodFormatter.stringFromDate(xValCountry.timeStamp!)
            
            xVals.append("\(dateString)")
            let zikaCountries = NetworkManager.sharedInstance.zikaCountries[i] as ZikaCountries?
            let zikaCountryCount = zikaCountries?.zikaCountriesCountries.count
            yVals.append(ChartDataEntry(value: Double(zikaCountryCount!), xIndex: i))
            latestCountryCount = Int(zikaCountryCount!)
        }
        countriesTotalLbl.text = "\(latestCountryCount) total countries reporting Zika"
        
        var set1 = LineChartDataSet()
        
        set1 = LineChartDataSet(yVals: yVals, label: "Total countries with active zika cases")
        set1.lineWidth = 3
        set1.setColor(UIColor.whiteColor())
        set1.circleRadius = 3
        set1.circleColors = [UIColor.whiteColor()]
        set1.fillColor = UIColor.whiteColor()
        set1.drawFilledEnabled = true
        set1.fillAlpha = 0.2
        
        let data:LineChartData = LineChartData(xVals: xVals, dataSet: set1)
        chartView.data = data
        
        for set in chartView.data!.dataSets
        {
            //set..drawCubicEnabled = true
            set.drawValuesEnabled = false
            (set as! ILineChartDataSet).mode = .CubicBezier//.drawCubicEnabled = true
        }
        chartView.setNeedsDisplay()
    }
    
    func drawDataForTotalPregnancies() {
        var yVals = [ChartDataEntry]()
        var xVals = [String?]()
        for i in 0...NetworkManager.sharedInstance.zikaPregnancyCases.count - 1 {
            let pregnancyCase = NetworkManager.sharedInstance.zikaPregnancyCases[i]
            yVals.append(ChartDataEntry(value: Double(pregnancyCase!.totalPregnancyCasesTerritory), xIndex: i))
               
            let dayTimePeriodFormatter = NSDateFormatter()
            dayTimePeriodFormatter.dateFormat = "M/d/yy"
            let dateString = dayTimePeriodFormatter.stringFromDate(pregnancyCase!.timeStamp!)
            xVals.append("\(dateString)")
        }
        
        var set1 = LineChartDataSet()
        set1 = LineChartDataSet(yVals: yVals, label: "Total local cases in the US")
        set1.lineWidth = 3
        set1.valueColors = [UIColor.clearColor()]
        set1.setColor(UIColor.whiteColor())
        set1.circleRadius = 3
        set1.circleColors = [UIColor.whiteColor()]
        set1.fillColor = UIColor.whiteColor()
        set1.drawFilledEnabled = true
        set1.fillAlpha = 0.2
        set1.mode = .CubicBezier
        
        let data:LineChartData = LineChartData(xVals: yVals, dataSet: set1)
        pregnancyChartView.data = data
        pregnancyChartView.setNeedsDisplay()
    }
    
    func drawDataForTotalUSCases() {
        var yVals = [BarChartDataEntry]()
        var yValsTravel = [BarChartDataEntry]()
        var xVals = [String?]()
        var historicalTotalLocalCases = 0
        var historicalTotalTravelCases = 0
        for i in 0...NetworkManager.sharedInstance.unitedStatesCases.count - 1 {
            let currentCountry = NetworkManager.sharedInstance.unitedStatesCases[i]
            var totalLocalCases:Double = 0
            var totalTravelCases:Double = 0
            for t in currentCountry!.territories {
                if let cases = (t! as Territory).localCases {
                    totalLocalCases += cases
                }
                if let cases = (t! as Territory).travelCases {
                    totalTravelCases += cases
                }
            }
            historicalTotalLocalCases = Int(totalLocalCases)
            historicalTotalTravelCases = Int(totalTravelCases)
            yVals.append(BarChartDataEntry(value: totalLocalCases, xIndex: i))
            yValsTravel.append(BarChartDataEntry(value: totalTravelCases, xIndex: i))
            
            let dayTimePeriodFormatter = NSDateFormatter()
            dayTimePeriodFormatter.dateFormat = "M/d/yy"
            let dateString = dayTimePeriodFormatter.stringFromDate(currentCountry!.timeStamp!)
            xVals.append("\(dateString)")
        }
        
        usLocalCasesTotalLbl.text = "\(historicalTotalLocalCases) locally transmitted cases (USA)"
        usTravelCasesTotalLbl.text = "\(historicalTotalTravelCases) travel related cases (USA)"
        
        var set1 = BarChartDataSet()
        var set2 = BarChartDataSet()
        
        set1 = BarChartDataSet(yVals: yVals, label: "Total local cases in the US")
        set1.isStacked
        set1.barShadowColor = UIColor.whiteColor()
        set1.setColor(UIColor.whiteColor())
        set1.valueTextColor = UIColor.clearColor()
        set1.valueFormatter = NSNumberFormatter()
        set1.valueFormatter?.numberStyle = .NoStyle
        
        let data:BarChartData = BarChartData(xVals: xVals, dataSet: set1)
        barChartView.data = data
        barChartView.setNeedsDisplay()
        
        set2 = BarChartDataSet(yVals: yValsTravel, label: "Total travel cases in the US")
        set2.isStacked
        set2.barShadowColor = UIColor.whiteColor()
        set2.setColor(UIColor.whiteColor())
        set2.valueTextColor = UIColor.clearColor()
        set2.valueColors = [UIColor.clearColor()]
        set2.valueFormatter = NSNumberFormatter()
        set2.valueFormatter?.numberStyle = .NoStyle
        
        let dataTravel:BarChartData = BarChartData(xVals: xVals, dataSet: set2)
        barChartViewTravel.data = dataTravel
        barChartViewTravel.setNeedsDisplay()
    }
    
    func chartValueSelected(chartView: ChartViewBase, entry: ChartDataEntry, dataSetIndex: Int, highlight: ChartHighlight) {
        //self.chartView.centerViewToAnimated(xIndex: entry.x, yValue: entry.y, axis: <#T##ChartYAxis.AxisDependency#>, duration: <#T##NSTimeInterval#>)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
