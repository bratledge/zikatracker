//
//  Territory.swift
//  ZikaTracker
//
//  Created by Administrator on 8/3/16.
//  Copyright © 2016 Bryan Ratledge. All rights reserved.
//

import UIKit

class Territory {
    var name: String?
    var localCases: Double?
    var travelCases: Double?
    var localPercentStr: String?
    var travelPercentStr: String?
    var activeWithZika: Bool = false
    var latVal: Double?
    var longVal: Double?
    var timeStamp: NSDate?
}