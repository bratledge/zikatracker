//
//  ZikaCountriesCountry.swift
//  ZikaTracker
//
//  Created by Administrator on 8/7/16.
//  Copyright © 2016 Bryan Ratledge. All rights reserved.
//

import UIKit

class ZikaCountriesCountry {
    var name: String?
    var continent: String?
    var latVal: Double?
    var longVal: Double?
}
