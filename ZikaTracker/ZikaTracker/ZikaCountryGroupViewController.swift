//
//  ZikaCountryGroupViewController.swift
//  ZikaTracker
//
//  Created by Administrator on 9/21/16.
//  Copyright © 2016 Bryan Ratledge. All rights reserved.
//

import UIKit
import FBAnnotationClustering

class ZikaCountryGroupViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var annotations: [AnyObject]?
    var countries: [ZikaCountriesCountry]? = [ZikaCountriesCountry]()
    var territories: [Territory]? = [Territory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        for i in self.annotations! {
            if i.isKindOfClass(ZikaMapAnnotation) {
                print("country")
                self.countries?.append((i as! ZikaMapAnnotation).country!)
            } else if i.isKindOfClass(ZikaStateMapAnnotation) {
                print("state")
                self.territories?.append((i as! ZikaStateMapAnnotation).territory!)
            }
        }
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countries!.count + self.territories!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if (self.annotations![indexPath.row].isKindOfClass(ZikaMapAnnotation)){
            let cell = tableView.dequeueReusableCellWithIdentifier("countryGroupCell", forIndexPath: indexPath) as UITableViewCell
            let countryLbl = cell.contentView.viewWithTag(2) as! UILabel
            countryLbl.text = (self.annotations![indexPath.row] as! ZikaMapAnnotation).country?.name!
            return cell
        } else if (self.annotations![indexPath.row].isKindOfClass(ZikaStateMapAnnotation)){
            let cell = tableView.dequeueReusableCellWithIdentifier("stateGroup", forIndexPath: indexPath) as UITableViewCell
            let annotation = ((self.annotations![indexPath.row]) as! ZikaStateMapAnnotation)
            let stateLbl = cell.contentView.viewWithTag(2) as! UILabel
            stateLbl.text = (self.annotations![indexPath.row] as! ZikaStateMapAnnotation).territory?.name!
            let localCasesLbl = cell.contentView.viewWithTag(3) as! UILabel
            localCasesLbl.text = "\(annotation.territory!.localCases!)"
            let travelCasesLbl = cell.contentView.viewWithTag(4) as! UILabel
            if (annotation.territory!.travelCases != nil) {
                travelCasesLbl.text = "\(annotation.territory!.travelCases!)"
            } else {
                travelCasesLbl.text = "0"
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
