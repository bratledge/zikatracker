//
//  ZikaCountryPopOverViewController.swift
//  ZikaTracker
//
//  Created by Administrator on 9/21/16.
//  Copyright © 2016 Bryan Ratledge. All rights reserved.
//

import UIKit

class ZikaCountryPopOverViewController: UIViewController {

    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var zikaMsgLbl: UILabel!
    var country: ZikaCountriesCountry?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.countryLbl.text = ""
        self.zikaMsgLbl.text = ""
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.countryLbl.text = "\(country!.name!)"
        self.zikaMsgLbl.text = "Active Zika Transmission"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
