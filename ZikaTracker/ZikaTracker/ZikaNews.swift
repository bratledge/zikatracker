//
//  ZikaNews.swift
//  ZikaTracker
//
//  Created by Administrator on 8/26/16.
//  Copyright © 2016 Bryan Ratledge. All rights reserved.
//

import UIKit

class ZikaNews {
    var title: String?
    var img_url: String?
    var map_image: String?
    var web_url: String?
    var type: String?
    var timeStamp: NSDate?
}
