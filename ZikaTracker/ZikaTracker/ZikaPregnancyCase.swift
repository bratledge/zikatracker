//
//  ZikaPregnancyCase.swift
//  ZikaTracker
//
//  Created by Administrator on 8/26/16.
//  Copyright © 2016 Bryan Ratledge. All rights reserved.
//

import UIKit

class ZikaPregnancyCase {
    var totalPregnancyCasesUSA: Int = 0
    var totalPregnancyCasesTerritory: Int = 0
    var timeStamp: NSDate?
}
