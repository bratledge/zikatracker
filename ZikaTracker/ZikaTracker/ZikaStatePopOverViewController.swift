//
//  ZikaStatePopOverViewController.swift
//  ZikaTracker
//
//  Created by Administrator on 9/21/16.
//  Copyright © 2016 Bryan Ratledge. All rights reserved.
//

import UIKit

class ZikaStatePopOverViewController: UIViewController {

    @IBOutlet weak var localCasesLbl: UILabel!
    @IBOutlet weak var travelCasesLbl: UILabel!
    @IBOutlet weak var stateNameLbl: UILabel!
    var territory: Territory?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.localCasesLbl.text = ""
        self.travelCasesLbl.text = ""
        self.stateNameLbl.text = ""
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.localCasesLbl.text = "\(territory!.localCases!)"
        self.travelCasesLbl.text = "\(territory!.travelCases!)"
        self.stateNameLbl.text = "\(territory!.name!)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
