//
//  ZikaTabBarViewController.swift
//  ZikaTracker
//
//  Created by Administrator on 8/11/16.
//  Copyright © 2016 Bryan Ratledge. All rights reserved.
//

import UIKit

class ZikaTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor = UIColor.whiteColor()
        self.tabBar.barTintColor = UIColor.blackColor()
        self.tabBar.backgroundImage = UIImage(contentsOfFile: "")
        
        
        
        var selectedImage = UIImage(named: "ebola_tab1")
        var unselectedImage = UIImage(named: "tab1a")
        let tabBar = self.tabBar
        
        let item1:UITabBarItem = tabBar.items![0]
        item1.image = unselectedImage?.imageWithRenderingMode(.AlwaysOriginal)
        item1.selectedImage = selectedImage?.imageWithRenderingMode(.AlwaysOriginal)

        selectedImage = UIImage(named: "ebola_tab2")
        unselectedImage = UIImage(named: "tab2a")
        
        let item2:UITabBarItem = tabBar.items![1]
        item2.image = unselectedImage?.imageWithRenderingMode(.AlwaysOriginal)
        item2.selectedImage = selectedImage?.imageWithRenderingMode(.AlwaysOriginal)
        
        selectedImage = UIImage(named: "ebola_tab3")
        unselectedImage = UIImage(named: "tab3a")
        
        let item3:UITabBarItem = tabBar.items![2]
        item3.image = unselectedImage?.imageWithRenderingMode(.AlwaysOriginal)
        item3.selectedImage = selectedImage?.imageWithRenderingMode(.AlwaysOriginal)
        
        selectedImage = UIImage(named: "ebola_tab4")
        unselectedImage = UIImage(named: "tab4a")
        
        let item4:UITabBarItem = tabBar.items![3]
        item4.image = unselectedImage?.imageWithRenderingMode(.AlwaysOriginal)
        item4.selectedImage = selectedImage?.imageWithRenderingMode(.AlwaysOriginal)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
